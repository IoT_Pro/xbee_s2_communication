# README
This is XBee S2 Wireless Communication Project with Raspberry Pi.

## XBee Pin Diagram
![XBeePin](image/XBee_pin.PNG)

## XBee - Raspberry Pi Connection
![XBee_RPi](image/XBee_RPi_connection.PNG)

## Build Interface
We should setup rpi interface for xbee S2 like this:

    sudo raspi-config
    sudo systemctl disable serial-getty@ttyAMA0.service
    sudo systemctl mask serial-getty@ttyAMA0.service    

